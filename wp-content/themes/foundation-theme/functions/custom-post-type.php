<?php
/* joints Custom Post Type Example
This page walks you through creating
a custom post type and taxonomies. You
can edit this one or copy the following code
to create another one.

I put this in a separate file so as to
keep it organized. I find it easier to edit
and change things if they are concentrated
in their own file.

*/


// Galerie
function fl_news_custom_post() {
    // creating (registering) the custom type
    register_post_type( 'aktualnosc', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
    // let's now add all the options for this post type
    array('labels' => array(
        'name' => __('Aktualności', 'jointswp'), /* This is the Title of the Group */
        'singular_name' => __('Aktualność', 'jointswp'), /* This is the individual type */
        'all_items' => __('Wszyskie aktualności', 'jointswp'), /* the all items menu item */
        'add_new' => __('Dodaj nową', 'jointswp'), /* The add new menu item */
        'add_new_item' => __('Dodaj nową aktualność', 'jointswp'), /* Add New Display Title */
        'edit' => __( 'Edytuj', 'jointswp' ), /* Edit Dialog */
        'edit_item' => __('Edytuj aktualność', 'jointswp'), /* Edit Display Title */
        'new_item' => __('Nowa aktualność', 'jointswp'), /* New Display Title */
        'view_item' => __('Wyświetl aktualność', 'jointswp'), /* View Display Title */
        'search_items' => __('Wyszukaj aktualność', 'jointswp'), /* Search Custom Type Title */
        'not_found' =>  __('Nie znaleziono w bazie.', 'jointswp'), /* This displays if there are no entries yet */
        'not_found_in_trash' => __('Nie znaleziono w koszu.', 'jointswp'), /* This displays if there is nothing in the trash */
        'parent_item_colon' => ''
    ), /* end of arrays */
    'public' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => false,
    'show_ui' => true,
    'query_var' => true,
    'menu_position' => 20, /* this is what order you want it to appear in on the left hand side menu */
    'menu_icon' => 'dashicons-megaphone', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
    'rewrite'	=> array( 'slug' => 'aktualnosci', 'with_front' => false ), /* you can specify its url slug */
    'has_archive' => 'aktualnosci', /* you can rename the slug here */
    // 'capability_type' => array( 'galeria', 'galerie' ),
    // 'capabilities' => array(
    //     'publish_posts' => 'publish_galerie',
    //     'edit_posts' => 'edit_galerie',
    //     'edit_others_posts' => 'edit_others_galerie',
    //     'delete_posts' => 'delete_galerie',
    //     'delete_others_posts' => 'delete_others_galerie',
    //     'read_private_posts' => 'read_private_galerie',
    //     'edit_post' => 'edit_galeria',
    //     'delete_post' => 'delete_galeria',
    //     'read_post' => 'read_galeria',
    // ),
    'hierarchical' => false,
    /* the next one is important, it tells what's enabled in the post editor */
    'supports' => array( 'title', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky', 'editor')
) /* end of options */
); /* end of register post type */

/* this adds your post categories to your custom post type */
register_taxonomy_for_object_type('category', 'aktualnosc');
/* this adds your post tags to your custom post type */
register_taxonomy_for_object_type('post_tag', 'aktualnosc');
}

// adding the function to the Wordpress init
add_action( 'init', 'fl_news_custom_post');

// function po_add_admin_gallery_caps() {
//     $role = get_role( 'administrator');
//     $role->add_cap('publish_galerie');
//     $role->add_cap('edit_galerie');
//     $role->add_cap('edit_others_galerie');
//     $role->add_cap('delete_galerie');
//     $role->add_cap('delete_others_galerie');
//     $role->add_cap('read_private_galerie');
//     $role->add_cap('edit_galeria');
//     $role->add_cap('delete_galeria');
//     $role->add_cap('read_galeria');
// }
// add_action( 'admin_init', 'po_add_admin_gallery_caps');

// Wydarzenia
function fl_events_custom_post() {
    // creating (registering) the custom type
    register_post_type( 'wydarzenie', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
    // let's now add all the options for this post type
    array('labels' => array(
        'name' => __('Wydarzenia', 'jointswp'), /* This is the Title of the Group */
        'singular_name' => __('Wydarzenie', 'jointswp'), /* This is the individual type */
        'all_items' => __('Wszyskie Wydarzenia', 'jointswp'), /* the all items menu item */
        'add_new' => __('Dodaj nowe', 'jointswp'), /* The add new menu item */
        'add_new_item' => __('Dodaj nowe Wydarzenie', 'jointswp'), /* Add New Display Title */
        'edit' => __( 'Edytuj', 'jointswp' ), /* Edit Dialog */
        'edit_item' => __('Edytuj Wydarzenie', 'jointswp'), /* Edit Display Title */
        'new_item' => __('Nowa Wydarzenie', 'jointswp'), /* New Display Title */
        'view_item' => __('Wyświetl Wydarzenie', 'jointswp'), /* View Display Title */
        'search_items' => __('Wyszukaj Wydarzenie', 'jointswp'), /* Search Custom Type Title */
        'not_found' =>  __('Nie znaleziono w bazie.', 'jointswp'), /* This displays if there are no entries yet */
        'not_found_in_trash' => __('Nie znaleziono w koszu.', 'jointswp'), /* This displays if there is nothing in the trash */
        'parent_item_colon' => ''
    ), /* end of arrays */
    'public' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => false,
    'show_ui' => true,
    'query_var' => true,
    'menu_position' => 20, /* this is what order you want it to appear in on the left hand side menu */
    'menu_icon' => 'dashicons-universal-access-alt', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
    'rewrite'	=> array( 'slug' => 'wydarzenia', 'with_front' => false ), /* you can specify its url slug */
    'has_archive' => 'wydarzenia', /* you can rename the slug here */
    // 'capability_type' => array( 'wydarzenia', 'wydarzenie' ),
    // 'capabilities' => array(
    //     'publish_posts' => 'publish_wydarzenia',
    //     'edit_posts' => 'edit_wydarzenia',
    //     'edit_others_posts' => 'edit_others_wydarzenia',
    //     'delete_posts' => 'delete_wydarzenia',
    //     'delete_others_posts' => 'delete_others_wydarzenia',
    //     'read_private_posts' => 'read_private_wydarzenia',
    //     'edit_post' => 'edit_wydarzenie',
    //     'delete_post' => 'delete_wydarzenie',
    //     'read_post' => 'read_wydarzenie',
    // ),
    'hierarchical' => false,
    /* the next one is important, it tells what's enabled in the post editor */
    'supports' => array( 'title', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky', 'editor')
) /* end of options */
); /* end of register post type */

/* this adds your post categories to your custom post type */
register_taxonomy_for_object_type('category', 'wydarzenie');
/* this adds your post tags to your custom post type */
register_taxonomy_for_object_type('post_tag', 'wydarzenie');
}

// adding the function to the Wordpress init
add_action( 'init', 'fl_events_custom_post');

// function po_add_admin_events_caps() {
//     $role = get_role( 'administrator');
//     $role->add_cap('publish_wydarzenia');
//     $role->add_cap('edit_wydarzenia');
//     $role->add_cap('edit_others_wydarzenia');
//     $role->add_cap('delete_wydarzenia');
//     $role->add_cap('delete_others_wydarzenia');
//     $role->add_cap('read_private_wydarzenia');
//     $role->add_cap('edit_wydarzenie');
//     $role->add_cap('delete_wydarzenie');
//     $role->add_cap('read_wydarzenie');
// }
// add_action( 'admin_init', 'po_add_admin_events_caps');


/*
looking for custom meta boxes?
check out this fantastic tool:
https://github.com/CMB2/CMB2
*/
