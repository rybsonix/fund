<?php
function site_scripts() {
  global $wp_styles; // Call global $wp_styles variable to add conditional wrapper around ie stylesheet the WordPress way

    // Adding scripts file in the footer
    wp_enqueue_script( 'swiper', '//cdnjs.cloudflare.com/ajax/libs/Swiper/5.3.8/js/swiper.min.js', array( 'jquery' ), filemtime(get_template_directory() . '/assets/scripts/js'), true);
    wp_enqueue_script( 'moment', '//cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/locale/pl.js', array( 'jquery' ), filemtime(get_template_directory() . '/assets/scripts/js'), true);
    wp_enqueue_script( 'fullcalendar', '//unpkg.com/@fullcalendar/core@4.3.1/main.min.js', array( 'jquery' ), filemtime(get_template_directory() . '/assets/scripts/js'), true);
    wp_enqueue_script( 'fullcalendar-daygrid', '//unpkg.com/@fullcalendar/daygrid@4.3.0/main.min.js', array( 'jquery' ), filemtime(get_template_directory() . '/assets/scripts/js'), true);
    wp_enqueue_script( 'fullcalendar-interaction', '//unpkg.com/@fullcalendar/interaction@4.3.0/main.min.js', array( 'jquery' ), filemtime(get_template_directory() . '/assets/scripts/js'), true);

    wp_enqueue_script( 'site-js', get_template_directory_uri() . '/assets/scripts/scripts.js', array( 'jquery' ), filemtime(get_template_directory() . '/assets/scripts/js'), true );

    // Register main stylesheet
    wp_enqueue_style( 'swiper', '//cdnjs.cloudflare.com/ajax/libs/Swiper/5.3.8/css/swiper.min.css', array(), filemtime(get_template_directory() . '/assets/styles/scss'), 'all');
    wp_enqueue_style( 'fullcalendar', '//unpkg.com/@fullcalendar/core@4.3.1/main.min.css', array(), filemtime(get_template_directory() . '/assets/styles/scss'), 'all');
    wp_enqueue_style( 'fullcalendar-daygrid', '//unpkg.com/@fullcalendar/daygrid@4.3.0/main.min.css', array(), filemtime(get_template_directory() . '/assets/styles/scss'), 'all');

    wp_enqueue_style( 'site-css', get_template_directory_uri() . '/assets/styles/style.css', array(), rand(111,9999), 'all' );

    // Comment reply script for threaded comments
    if ( is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
      wp_enqueue_script( 'comment-reply' );
    }
}
add_action('wp_enqueue_scripts', 'site_scripts', 999);
