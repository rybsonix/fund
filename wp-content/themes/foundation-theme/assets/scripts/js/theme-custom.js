(function( root, $, undefined ) {

    $(function () {

        // Swiper
        var heroThumbs = new Swiper('.hero-swiper__img', {
            slidesPerView: 1,
            loop: true,
            loopedSlides: 3, //looped slides should be the same
            watchSlidesVisibility: true,
            watchSlidesProgress: true,
            speed: 700
        });
        var heroMain = new Swiper('.hero-swiper__thumb', {
            loop:true,
            loopedSlides: 3, //looped slides should be the same
            thumbs: {
                swiper: heroThumbs,
            },
            pagination: {
                el: '.swiper-pagination',
                clickable: false,
                type: 'fraction',
                renderFraction: function (currentClass, totalClass) {
                    return '<span class="' + currentClass + '"></span>' +
                    ' z ' +
                    '<span class="' + totalClass + '"></span>';
                }
            },
            autoplay: {
                delay: 6000,
                disableOnInteraction: false
            },
            speed: 700,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
        });

        var charSlider = new Swiper('.character-swiper', {
            slidesPerView: 2,
            slidesPerColumn: 2,
            slidesPerColumnFill: 'row',
            spaceBetween: 13,
            breakpoints: {
                640: {
                    slidesPerView: 3,
                    spaceBetween: 20,
                },
                1024: {
                    slidesPerView: 4,
                    spaceBetween: 25,
                }
            }
        });

        // Sticky Header
        var stickyHeader = $('.header-bottom').offset().top;
        var stickyHeaderMobile = $('.header-top').offset().top;
        $(window).scroll(function(){
            if(window.location.pathname != "/wydarzenia-kalendarz/" && $(document).height() > 1300){
                if($(window).width() >= 1024){
                    if( $(window).scrollTop() > stickyHeader ) {
                        $('.header-bottom').css({position: 'fixed', top: '0px'}).addClass('sticky-header');
                    } else {
                        $('.header-bottom').css({position: 'relative', top: '0px'}).removeClass('sticky-header');
                    }
                } else {
                    if( $(window).scrollTop() > stickyHeaderMobile ) {
                        $('.header-top').css({position: 'fixed', top: '0px'}).addClass('sticky-header');
                    } else {
                        $('.header-top').css({position: 'relative', top: '0px'}).removeClass('sticky-header');
                    }
                }
            }
        });

        // Sticky Overlap Events
        $('.info-banner-overlap').click(function(){
            $(this).parents('.info-banner-container').toggleClass('active');
        });

        $(document).click(function() {
            $('.info-banner-container').removeClass('active');
        });

        $('.info-banner').click(function(e) {
            e.stopPropagation();
        });

        $('.is-dropdown-submenu-parent').hover(function() {
            $(this).removeClass('opens-inner');
        });

        // Google Map Javascript Api
        if ($("#map").length > 0){
            function customMap() {
                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 17,
                    center: {lat: 52.40481, lng: 16.94014}
                });
                var image = '/wp-content/themes/foundation-theme/assets/images/marker.svg';
                var marker = new google.maps.Marker({
                    position: {lat: 52.40481, lng: 16.94014},
                    map: map,
                    icon: image
                });
                marker.setCursor('grab');
            };
            google.maps.event.addDomListener(window, 'load', customMap);
        }

        if(window.location.pathname == '/o-patronie/'){
            $('.top-bar-right__translate a').click(function(){
                $('.single-page__text.single-page-patron__text').toggleClass('active');
                $('.single-page__text-en.single-page-patron__text-en').toggleClass('active');
                $('.single-page-patron .single-rows__text').toggleClass('active');
                $('.single-page-patron .single-rows__text-en').toggleClass('active');
            })
        }

    });

} ( this, jQuery ));
