<?php
/**
* The main template file
*
* This is the most generic template file in a WordPress theme
* and one of the two required files for a theme (the other being style.css).
* It is used to display a page when nothing more specific matches a query.
*/

get_header(); ?>

<div class="content blog">

	<header class="archive-header">
		<div class="grid-container">
			<h1>Blog</h1>
		</div>
	</header>

	<div class="grid-container">

		<main class="main" role="main">

			<div class="inner-content grid-x grid-margin-x">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<!-- To see additional archive styles, visit the /parts directory -->
					<?php get_template_part( 'parts/loop', 'archive' ); ?>

				<?php endwhile; ?>

				<?php joints_page_navi(); ?>

				<?php else : ?>

					<?php get_template_part( 'parts/content', 'missing' ); ?>

				<?php endif; ?>

			</div> <!-- end #inner-content -->

		</main> <!-- end #main -->

	</div>

</div> <!-- end #content -->

<?php get_footer(); ?>
