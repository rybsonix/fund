<?php
/**
 * The template for displaying search results pages
 *
 * For more info: https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 */

get_header(); ?>

	<div class="content search-page">

        <header class="archive-header">
            <div class="grid-container">
                <h1><?php _e( 'Search Results for:', 'jointswp' ); ?> <?php echo esc_attr(get_search_query()); ?></h1>
            </div>
        </header>

        <div class="grid-container">

			<main class="main" role="main">

                <div class="inner-content grid-x grid-margin-x">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<!-- To see additional archive styles, visit the /parts directory -->
					<?php get_template_part( 'parts/loop', 'archive' ); ?>

				<?php endwhile; ?>

					<?php joints_page_navi(); ?>

				<?php else : ?>

					<?php get_template_part( 'parts/content', 'missing' ); ?>

			    <?php endif; ?>

                </div> <!-- end #inner-content -->

		    </main> <!-- end #main -->

        </div>

	</div> <!-- end #content -->

<?php get_footer(); ?>
