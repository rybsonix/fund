<?php
/*
Template Name: Podstrona - Wydarzenia - Kalendarz
*/

get_header(); ?>

<div class="content single-page events-calendar">

	<div class="inner-content grid-x grid-margin-x">

		<main class="main small-12 cell" role="main">

		    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		    	<?php get_template_part( 'parts/loop', 'events-calendar' ); ?>

		    <?php endwhile; else : ?>

		   		<?php get_template_part( 'parts/content', 'missing' ); ?>

		    <?php endif; ?>

		</main> <!-- end #main -->

	</div> <!-- end #inner-content -->

</div> <!-- end #content -->

<?php get_footer(); ?>
