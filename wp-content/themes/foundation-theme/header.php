<?php
/**
* The template for displaying the header
*
* This is the template that displays all of the <head> section
*
*/
?>

<!doctype html>

<html class="no-js"  <?php language_attributes(); ?>>

<head>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-170178354-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-170178354-1');
    </script>
    <meta charset="utf-8">

    <!-- Force IE to use the latest rendering engine available -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta class="foundation-mq">

    <!-- If Site Icon isn't set in customizer -->
    <?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) { ?>
        <!-- Icons & Favicons -->

        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/assets/images/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon-16x16.png">
        <link rel="mask-icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="theme-color" content="#ffffff">
    <?php } ?>

    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC66vfr9zslaEwg_-NTO9OAhUSNWU63JcM" async defer></script>
    <?php wp_head(); ?>

</head>

<?php
$image = get_field('main_logo','option');
?>

<body <?php body_class(); ?>>

    <div class="off-canvas-wrapper">

        <!-- Load off-canvas container. Feel free to remove if not using. -->
        <?php get_template_part( 'parts/content', 'offcanvas' ); ?>

        <div class="off-canvas-content" data-off-canvas-content>

            <header class="header" role="banner">

                <?php
                $args = array(
                    'post_type' => 'wydarzenie',
                    'posts_per_page' => -1,
                    'meta_query' => array(
                        array(
                            'key' => 'important_event',
                            'value' => '1',
                            'compare' => '==' // not really needed, this is the default
                        )
                    )
                );
                $the_query = new WP_Query( $args ); ?>

                <?php if ( $the_query->have_posts() ) : ?>
                    <div class="info-banner-container active">
                        <div class="info-banner card">
                            <h2>skrót <br />do eventów</h2>
                            <!-- the loop -->
                            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                                <?php
                                $image_event = get_field('important_event_logo');
                                ?>
                                <a href="<?php the_permalink(); ?>"> <img src="<?php echo esc_url($image_event['url']); ?>" alt="<?php echo esc_attr($image_event['alt']); ?>" /></a>
                            <?php endwhile; ?>
                            <!-- end of the loop -->
                            <?php wp_reset_postdata(); ?>
                            <div class="info-banner-overlap">
                                <span>|</span>
                                <span>|</span>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>

                <div class="grid-container">

                    <nav class="header-top">

                        <div class="top-bar-left">
                            <ul class="menu">
                                <li><a href="<?php echo home_url(); ?>"><img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>"></a></li>
                            </ul>
                        </div>
                        <div class="top-bar-right show-for-large">
                            <?php if ( is_active_sidebar( 'hp-header-sidebar' ) ) : ?>

                                <?php dynamic_sidebar( 'hp-header-sidebar' ); ?>

                            <?php endif; ?>
                            <div class="top-bar-right__translate">
                                <?php echo do_shortcode('[gtranslate]'); ?>
                            </div>
                        </div>
                        <div class="top-bar-right hide-for-large">
                            <div class="top-bar-right__translate">
                                <?php echo do_shortcode('[gtranslate]'); ?>
                            </div>
                            <ul class="menu">
                                <li><button class="menu-icon" type="button" data-toggle="off-canvas"></button></li>
                                <!-- <li><a data-toggle="off-canvas"><?php _e( 'Menu', 'jointswp' ); ?></a></li> -->
                            </ul>
                        </div>
                    </nav>
                </div>
                <nav class="header-bottom">
                    <?php get_template_part( 'parts/nav', 'offcanvas-topbar' ); ?>
                </nav>

            </header> <!-- end .header -->
