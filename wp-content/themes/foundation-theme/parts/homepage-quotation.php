<section class="home-quotation secondary-bg">
    <div class="grid-container">
        <div class="grid-x">
            <div class="cell small-12">
                <div class="home-quotation__box">
                    <?php the_content();?>
                    <p class="home-quotation__author"><strong><?php the_field('home_quot_auth');?></strong></p>
                </div>
            </div>
        </div>
    </div>
</section>
