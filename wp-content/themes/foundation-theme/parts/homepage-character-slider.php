<section class="home-characters secondary-bg">
    <div class="grid-container">
        <h2>Nasze autorytety</h2>
        <div class="grid-x grid-margin-x">
            <div class="small-12 cell">
                <div class="swiper-container character-swiper">
                    <?php if( have_rows('home_characters') ): ?>
                    <div class="swiper-wrapper">
                    <?php while( have_rows('home_characters') ): the_row();?>
                        <?php
                        $image = get_sub_field('home_character_img');
                        $name = get_sub_field('home_character_name');
                        ?>
                        <?php if(!empty($image) && !empty($name)):?>
                        <div class="swiper-slide">
                            <div class="home-characters__img">
                                <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                            </div>
                            <div class="home-characters__text">
                                <p><?php echo $name;?></p>
                            </div>
                        </div>
                        <?php endif; ?>
                    <?php endwhile; ?>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
