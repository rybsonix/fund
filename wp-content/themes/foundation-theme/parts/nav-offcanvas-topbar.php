<?php
/**
 * The off-canvas menu uses the Off-Canvas Component
 *
 * For more info: http://jointswp.com/docs/off-canvas-menu/
 */
$image_mobile = get_field('main_logo_mobile','option');
?>

<div class="top-bar" id="top-bar-menu">
	<div class="show-for-large">
		<div class="grid-container">
			<div class="mobile-logo">
				<a href="<?php echo home_url(); ?>"><img src="<?php echo esc_url($image_mobile['url']); ?>" alt="<?php echo esc_attr($image_mobile['alt']); ?>"></a>
			</div>
			<?php joints_top_nav(); ?>
		</div>
	</div>
</div>
