<?php
/**
 * Template part for displaying posts
 *
 * Used for single, index, archive, search.
 */
?>

<div class="small-12 medium-6 cell">

	<article class="section-main section-main--archive" id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article">

		<div class="card section-main__card">
			<div class="home-news__img section-main__img">
				<?php
				$thumbnail_id = get_post_thumbnail_id( $post->ID );
				$alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);
				$image = get_field('main_logo','option');
				if($thumbnail_id):
					the_post_thumbnail( 'full', array( 'alt' => $alt ) );
				else: ?>
					<img class="default-cover" src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>">
				<?php endif;?>
			</div>
			<div class="home-news__text section-main__text">
				<h3><?php the_title(); ?></h3>
				<?php fl_excerpt($post->ID, 'fl_archive_short'); ?>
				<a class="button" href="<?php the_permalink(); ?>">zobacz</a>
			</div>
		</div>

	</article> <!-- end article -->

</div>
