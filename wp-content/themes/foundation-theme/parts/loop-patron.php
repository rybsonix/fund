<?php
/**
* Template part for displaying page content in page.php
*/
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/WebPage">

	<header class="archive-header">
		<div class="grid-container">
			<h1><?php the_title(); ?></h1>
		</div>
	</header> <!-- end article header -->

	<section class="entry-content" itemprop="text">
		<div class="single-page-patron-main">
			<div class="grid-container">
				<div class="grid-x grid-margin-x grid-align-bottom">
					<?php
					$thumbnail_id = get_post_thumbnail_id( $post->ID );
					$alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);
					?>
					<div class="small-12 medium-7 cell">
						<?php
						$actual_link = "$_SERVER[REQUEST_URI]";
						if($actual_link == "/o-patronie/"){
							$cookie_name = "googtrans";
							$cookie_value = "/pl/en";
							if(!isset($_COOKIE[$cookie_name])) {
								$pl_class = "active";
								$en_class = "";
							} else {
								$pl_class = "";
								$en_class = "active";
							}
						}
						?>
						<div class="single-page__text single-page-patron__text <?php echo $pl_class;?>">
							<?php the_content(); ?>
						</div>
						<div class="single-page__text-en single-page-patron__text-en <?php echo $en_class;?>">
							<?php the_field('translate_main_text'); ?>
						</div>
					</div>
					<div class="small-12 medium-5 cell">
						<div class="single-page-patron__thumb">
							<?php the_post_thumbnail( 'full', array( 'alt' => $alt ) ); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php if( have_rows('content_txtimg') ): ?>
			<div class="single-rows">
				<?php

				while( have_rows('content_txtimg') ): the_row(); ?>

				<?php $content_txt = get_sub_field('content_txt');
				if($content_txt):?>
				<div class="single-rows__text <?php echo $pl_class;?>">
					<div class="grid-container grid-container--small">
						<?php echo $content_txt; ?>
					</div>
				</div>
			<?php endif; ?>
				<div class="single-rows__text-en <?php echo $en_class;?>">
					<div class="grid-container grid-container--small">
						<?php the_field('translate_content_text'); ?>
					</div>
				</div>

			<?php $image_full = get_sub_field('content_img_full');
			if($image_full):?>
			<div class="single-rows__img-full">
				<div class="grid-container">
					<img src="<?php echo esc_url($image_full['url']); ?>" alt="<?php echo esc_attr($image_full['alt']); ?>" />
				</div>
			</div>
		<?php endif; ?>

		<?php if( have_rows('content_img_half') ): ?>
			<div class="single-rows__img-half">
				<div class="grid-container">
					<div class="grid-x grid-margin-x grid-margin-y">
						<?php while( have_rows('content_img_half') ): the_row();
						$image_half = get_sub_field('content_img_half_item');
						?>
						<div class="small-12 medium-6 cell">
							<img src="<?php echo esc_url($image_half['url']); ?>" alt="<?php echo esc_attr($image_half['alt']); ?>" />
						</div>
					<?php endwhile; ?>
				</div>
			</div>
		</div>
	<?php endif;?>

	<?php $content_txt_full = get_sub_field('content_txt_full');
	if($content_txt_full):?>
	<div class="single-rows__text">
		<div class="grid-container">
			<?php echo $content_txt_full; ?>
		</div>
	</div>
<?php endif; ?>

<?php endwhile; ?>
</div>
<?php endif;?>
</section> <!-- end article section -->

<footer class="article-footer">
	<?php wp_link_pages(); ?>
</footer> <!-- end article footer -->

</article> <!-- end article -->
