<?php
/**
* Template part for displaying page content in page.php
*/
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/WebPage">

	<header class="archive-header">
		<div class="grid-container">
			<h1><?php the_title(); ?></h1>
		</div>
	</header> <!-- end article header -->

	<section class="entry-content" itemprop="text">
		<div class="grid-container">
			<div class="grid-x grid-margin-x">
				<?php
				$thumbnail_id = get_post_thumbnail_id( $post->ID );
				$alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);
				?>
					<div class="small-12 large-5 cell">
						<div class="single-page__text">
							<?php the_content(); ?>
							<?php
							$link_face = get_field('contact_facebook_link');
							$link_insta = get_field('contact_instagram_link');
							$link_twit = get_field('contact_twitter_link');
							?>
							<ul>
								<?php if($link_face):?>
									<?php
									$link_face_url = $link_face['url'];
									$link_face_title = $link_face['title'];
									$link_face_target = $link_face['target'] ? $link_face['target'] : '_self';
									?>
								<li><a href="<?php echo esc_url( $link_face_url ); ?>" target="<?php echo esc_attr( $link_face_target ); ?>"><i class="fab fa-facebook-f"></i><?php echo esc_html( $link_face_title ); ?></a></li>
								<?php endif; ?>

								<?php if($link_insta):?>
									<?php
									$link_insta_url = $link_insta['url'];
									$link_insta_title = $link_insta['title'];
									$link_insta_target = $link_insta['target'] ? $link_insta['target'] : '_self';
									?>
								<li><a href="<?php echo esc_url( $link_insta_url ); ?>" target="<?php echo esc_attr( $link_insta_target ); ?>"><i class="fab fa-instagram"></i><?php echo esc_html( $link_insta_title ); ?></a></li>
								<?php endif; ?>

								<?php if($link_twit):?>
									<?php
									$link_twit_url = $link_twit['url'];
									$link_twit_title = $link_twit['title'];
									$link_twit_target = $link_twit['target'] ? $link_twit['target'] : '_self';
									?>
								<li><a href="<?php echo esc_url( $link_twit_url ); ?>" target="<?php echo esc_attr( $link_twit_target ); ?>"><i class="fab fa-twitter"></i><?php echo esc_html( $link_twit_title ); ?></a></li>
								<?php endif; ?>
							</ul>
						</div>
					</div>
					<div class="small-12 large-7 cell">
						<div class="single-page__map">
							<div id="map"></div>
						</div>
					</div>
				</div>
			</div>
			<?php if( have_rows('content_txtimg') ): ?>
				<div class="single-rows">
					<?php

					while( have_rows('content_txtimg') ): the_row(); ?>

					<?php $content_txt = get_sub_field('content_txt');
					if($content_txt):?>
					<div class="single-rows__text">
						<div class="grid-container grid-container--small">
							<?php echo $content_txt; ?>
						</div>
					</div>
				<?php endif; ?>

				<?php $image_full = get_sub_field('content_img_full');
				if($image_full):?>
				<div class="single-rows__img-full">
					<div class="grid-container">
						<img src="<?php echo esc_url($image_full['url']); ?>" alt="<?php echo esc_attr($image_full['alt']); ?>" />
					</div>
				</div>
			<?php endif; ?>

			<?php if( have_rows('content_img_half') ): ?>
				<div class="single-rows__img-half">
					<div class="grid-container">
						<div class="grid-x grid-margin-x grid-margin-y">
							<?php while( have_rows('content_img_half') ): the_row();
							$image_half = get_sub_field('content_img_half_item');
							?>
							<div class="small-12 medium-6 cell">
								<img src="<?php echo esc_url($image_half['url']); ?>" alt="<?php echo esc_attr($image_half['alt']); ?>" />
							</div>
						<?php endwhile; ?>
					</div>
				</div>
			</div>
		<?php endif;?>

		<?php $content_txt_full = get_sub_field('content_txt_full');
		if($content_txt_full):?>
		<div class="single-rows__text">
			<div class="grid-container">
				<?php echo $content_txt_full; ?>
			</div>
		</div>
	<?php endif; ?>

<?php endwhile; ?>
</div>
<?php endif;?>
</section> <!-- end article section -->

<footer class="article-footer">
	<?php wp_link_pages(); ?>
</footer> <!-- end article footer -->

</article> <!-- end article -->
