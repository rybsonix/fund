<?php
/**
* Template part for displaying page content in page.php
*/
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/WebPage">

	<header class="archive-header">
		<div class="grid-container">
			<div class="archive-header-calendar">
				<h1><?php the_title(); ?></h1>
				<a href="/wydarzenia" class="button button--small">zobacz wszystko</a>
			</div>
		</div>
	</header> <!-- end article header -->

	<section class="entry-content" itemprop="text">
		<div class="grid-container">
			<div class="grid-x grid-margin-x">
				<div class="small-12 large-8 cell">
					<div class="event-thumb"></div>
				</div>
				<div class="small-12 large-4 cell">
					<div id='calendar'></div>
				</div>
			</div>

			<script>
			var events = [
				<?php
				$args = array(
					'post_type' => 'wydarzenie',
					'posts_per_page' => -1,
					'order' => 'ASC'
				);
				$the_query = new WP_Query($args);
				while($the_query->have_posts()): $the_query->the_post();
				?>
				<?php
				$event_date = get_field('event_date_time');
				if($event_date):
					$event_format = explode('/', $event_date);
					$eventDate = $event_format[0];
					$eventTime = $event_format[1];
					?>
					{
						id: '<?php the_ID(); ?>',
						title: '<?php echo get_the_title(); ?>',
						start: '<?php echo date('Y-m-d', strtotime($eventDate));?>',
						url: '<?php the_permalink(); ?>',
						thumbnail: '<?php echo get_the_post_thumbnail_url(get_the_ID(),'large'); ?>',
						rendering: 'background',
						startHour: '<?php echo $eventTime; ?>'
					},
					<?php endif; ?>
					<?php
				endwhile;
				wp_reset_postdata();
				?>
			]

			jQuery(function () {

				var calendarEl = document.getElementById('calendar');

				var calendar = new FullCalendar.Calendar(calendarEl, {
					plugins: [ 'dayGrid' ],
					height: 'auto',
					firstDay: 1,
					fixedWeekCount: false,
					header: {
						left: 'prev',
						center: 'title',
						right: 'next'
					},
					titleFormat: {
						month: 'long',
						year: 'numeric'
					},
					locale: 'pl',
					editable: true,
					eventLimit: true,
					navLinks: true,
					today: 'dzisiaj',
					month: 'miesiąc',
					week: 'tydzień',
					day: 'dzień',
					events: events,
					columnHeaderText: function(date){
						if(date.getDay() === 0) return 'ND';
						else if(date.getDay() === 1) return 'PN';
						else if(date.getDay() === 2) return 'WT';
						else if(date.getDay() === 3) return 'ŚR';
						else if(date.getDay() === 4) return 'CZW';
						else if(date.getDay() === 5) return 'PT';
						else return 'SB';
					},
					navLinkDayClick: function(date, jsEvent) {
						date = moment(date).format('YYYY-MM-DD');
						var eventThumb = jQuery(".event-thumb");
						eventThumb.html('');
						var eventsArray = calendar.getEvents();
						for(i = 0; i < eventsArray.length; i++){
							var eventDateFromArray = moment(eventsArray[i].start).format('YYYY-MM-DD');
							var firstEventStart = moment(eventsArray[0].start).format('YYYY-MM-DD');
							var eventImgFromArray = eventsArray[i].extendedProps.thumbnail;
							var eventUrlFromArray = eventsArray[i].url;
							var eventTitleFromArray = eventsArray[i].title;
							var frontEventStart = moment(eventsArray[i].start).format('DD.MM.YYYY');
							var eventHourFromArray = ' godz.' + eventsArray[i].extendedProps.startHour;

							var visible = false;

							if(date == eventDateFromArray && eventDateFromArray == firstEventStart){
								visible = true;

								var eventItemContainer = jQuery('<div/>', {
									class: 'event-calendar-item',
								}).prependTo(eventThumb);

								var eventItemImg = jQuery('<div/>', {
									class: 'event-calendar-img',
								}).prependTo(eventItemContainer);

								var div = jQuery('<img/>', {
									src: eventImgFromArray
								}).prependTo(eventItemImg);

								var eventItemText = jQuery('<div/>', {
									class: 'event-calendar-text',
								}).appendTo(eventItemContainer);

								var frontDate = jQuery('<p/>', {
									html: frontEventStart
								}).appendTo(eventItemText);

								var aTitle = jQuery('<h3/>', {
									html: eventTitleFromArray
								}).appendTo(eventItemText);

								var readMore = jQuery('<a/>', {
									"href": eventUrlFromArray,
									class: 'button',
									html: 'zobacz'
								}).appendTo(eventItemText);

								// var hour = jQuery('<span/>', {
								// 	html: eventHourFromArray
								// }).appendTo(frontDate);

							}
							if(date == eventDateFromArray && eventDateFromArray != firstEventStart){

								visible = true;

								var eventItemContainer = jQuery('<div/>', {
									class: 'event-calendar-item',
								}).prependTo(eventThumb);

								var eventItemImg = jQuery('<div/>', {
									class: 'event-calendar-img',
								}).prependTo(eventItemContainer);

								var div = jQuery('<img/>', {
									src: eventImgFromArray
								}).prependTo(eventItemImg);

								var eventItemText = jQuery('<div/>', {
									class: 'event-calendar-text',
								}).appendTo(eventItemContainer);

								var frontDate = jQuery('<p/>', {
									html: frontEventStart
								}).appendTo(eventItemText);

								var aTitle = jQuery('<h3/>', {
									html: eventTitleFromArray
								}).appendTo(eventItemText);

								var readMore = jQuery('<a/>', {
									"href": eventUrlFromArray,
									class: 'button',
									html: 'zobacz'
								}).appendTo(eventItemText);

								// var hour = jQuery('<span/>', {
								// 	html: eventHourFromArray
								// }).appendTo(frontDate);

							}
							if(visible){
								eventThumb.show();
							}
						};
					},
					eventRender: function(info){
						var eventDate = moment(info.event.start).format('YYYY-MM-DD');
						var days = jQuery(".fc-day-grid td.fc-day-top");
						jQuery.each(days, function( index, value ) {
							var day = jQuery(value).data('date');
							if(day == eventDate){
								jQuery(value).find('.fc-day-number').addClass('has-event');
							}
						});
					},
					datesRender: function(view, element) {

						var eventThumb = jQuery(".event-thumb");
						eventThumb.html('');
						var calendarStart = moment(calendar.view.activeStart).format('YYYY-MM-DD');
						var calendarEnd = moment(calendar.view.activeEnd).format('YYYY-MM-DD');
						var now = moment(new Date()).format('YYYY-MM-DD');
						var eventsArray = calendar.getEvents();
						// var evntsCounter = eventsArray.length >= 3 ? 3 : eventsArray.length;

						for(i = 0; i < eventsArray.length; i++){
							var eventStart = moment(eventsArray[i].start).format('YYYY-MM-DD');
							var firstEventStart = moment(eventsArray[i].start).format('YYYY-MM-DD');
							var frontEventStart = moment(eventsArray[i].start).format('DD.MM.YYYY');
							var eventImgFromArray = eventsArray[i].extendedProps.thumbnail;
							var eventHourFromArray = ' godz.' + eventsArray[i].extendedProps.startHour;
							var eventUrlFromArray = eventsArray[i].url;
							var eventTitleFromArray = eventsArray[i].title;

							// if(now <= firstEventStart && eventStart == firstEventStart){
							if(firstEventStart >= calendarStart && firstEventStart <= calendarEnd){

								var eventItemContainer = jQuery('<div/>', {
									class: 'event-calendar-item',
								}).prependTo(eventThumb);

								var eventItemImg = jQuery('<div/>', {
									class: 'event-calendar-img',
								}).prependTo(eventItemContainer);

								var div = jQuery('<img/>', {
									src: eventImgFromArray
								}).prependTo(eventItemImg);

								var eventItemText = jQuery('<div/>', {
									class: 'event-calendar-text',
								}).appendTo(eventItemContainer);

								var frontDate = jQuery('<p/>', {
									html: frontEventStart
								}).appendTo(eventItemText);

								var aTitle = jQuery('<h3/>', {
									html: eventTitleFromArray
								}).appendTo(eventItemText);

								var readMore = jQuery('<a/>', {
									"href": eventUrlFromArray,
									class: 'button',
									html: 'zobacz'
								}).appendTo(eventItemText);

								// var hour = jQuery('<span/>', {
								// 	html: eventHourFromArray
								// }).appendTo(eventItemText);

							}
							eventThumb.show();
						}
					}
				});

				calendar.render();
			});
			</script>
		</div>
	</section> <!-- end article section -->

	<footer class="article-footer">
		<?php wp_link_pages(); ?>
	</footer> <!-- end article footer -->

</article> <!-- end article -->
