<?php
$args = array(
    'post_type' => array('post','aktualnosc','wydarzenie'),
    'posts_per_page' => 3,
);
$the_query = new WP_Query( $args ); ?>

<?php if ( $the_query->have_posts() ) : ?>
    <section class="hero-container">
        <div class="grid-container">
            <div class="hero-swiper">
                <div class="grid-x">
                    <div class="cell medium-12 large-7 left-item">
                        <div class="swiper-container hero-swiper__img">
                            <div class="swiper-wrapper">
                                <!-- the loop -->
                                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                                    <div class="swiper-slide">
                                        <div class="hero-swiper__img-slide">
                                            <?php
                                            $image = get_field('home_slide_img');
                                            ?>
                                            <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                                        </div>
                                    </div>
                                <?php endwhile; ?>
                                <!-- end of the loop -->
                            </div>
                        </div>
                    </div>
                    <div class="cell medium-12 large-6 right-item">
                        <div class="swiper-container hero-swiper__thumb">
                            <div class="swiper-wrapper">
                                <!-- the loop -->
                                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                                    <div class="swiper-slide">
                                        <div class="hero-swiper__thumb-desc">
                                            <h3><?php the_title(); ?></h3>
                                            <?php fl_excerpt($post->ID, 'fl_archive_medium'); ?>
                                            <a class="link button" href="<?php the_permalink(); ?>">Zobacz</a>
                                        </div>
                                    </div>
                                <?php endwhile; ?>
                            </div>
                            <div class="swiper-pagination"></div>
                            <div class="swiper-button-prev"><i class="fas fa-chevron-left"></i></div>
                            <div class="swiper-button-next"><i class="fas fa-chevron-right"></i></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php wp_reset_postdata(); ?>
<?php endif; ?>
