<?php
/**
* The template part for displaying offcanvas content
*
* For more info: http://jointswp.com/docs/off-canvas-menu/
*/
?>

<div class="off-canvas position-right" id="off-canvas" data-off-canvas data-transition="overlap">

	<button class="close-button" aria-label="Close menu" type="button" data-close>
		<span aria-hidden="true">&times;</span>
	</button>

	<?php joints_off_canvas_nav(); ?>

	<?php if ( is_active_sidebar( 'offcanvas' ) ) : ?>

		<?php dynamic_sidebar( 'offcanvas' ); ?>

	<?php endif; ?>

	<div class="off-canvas__bottom">

		<?php if ( is_active_sidebar( 'hp-header-sidebar' ) ) : ?>

			<?php dynamic_sidebar( 'hp-header-sidebar' ); ?>

		<?php endif; ?>

	</div>

</div>
