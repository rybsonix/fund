<?php
/**
* Template part for displaying page content in page.php
*/
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/WebPage">

	<header class="archive-header">
		<div class="grid-container">
			<h1><?php the_title(); ?></h1>
		</div>
	</header> <!-- end article header -->

	<section class="entry-content" itemprop="text">
		<div class="single-page-person-horizontal-main single-page-publish-horizontal-main single-page-publish-book-horizontal-main">
			<div class="grid-container">

				<?php if( have_rows('img_desc') ): ?>

					<div class="grid-x grid-margin-x">

						<?php $i = 0; ?>
						<?php $e = 0; ?>

						<?php while( have_rows('img_desc') ): the_row();

						// vars
						$image = get_sub_field('img_desc_img');
						$name = get_sub_field('img_desc_name');
						$desc = get_sub_field('img_desc_desc');
						$desc_more = get_sub_field('img_desc_desc_more');
						$title_desc = get_sub_field('img_desc_book_desc');
						?>

						<div class="small-12 cell">
							<div class="character-container-horizontal">
								<div class="character-horizontal-img-container">
									<div class="characters-horizontal-img characters-horizontal-img--small">
										<img data-open="reveal-<?php echo $i++;?>" src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
										<div class="reveal" id="reveal-<?php echo $e++;?>" data-reveal>
											<img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
											<button class="close-button" data-close aria-label="Close reveal" type="button">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
									</div>
								</div>
								<div class="characters-horizontal-desc-container">
									<div class="characters-horizontal-text">
										<p><?php echo $name;?></p>
										<p class="characters-horizontal-text__small"><?php echo $title_desc;?></p>
									</div>
									<div class="characters-horizontal-desc">
										<p><?php echo $desc;?></p>
									</div>
									<?php if($desc_more):?>
										<div class="characters-horizontal-desc-more">
											<ul class="accordion" data-accordion data-allow-all-closed="true">
												<li class="accordion-item" data-accordion-item>
													<div class="accordion-content" data-tab-content>
														<p><?php echo $desc_more;?></p>
													</div>
													<a href="#" class="accordion-close accordion-title"><img src="<?php echo get_template_directory_uri() . '/assets/images/arrow-down.svg';?>" alt="Arrow down icon">Rozwiń</a>
													<a href="#" class="accordion-open accordion-title"><img src="<?php echo get_template_directory_uri() . '/assets/images/arrow-up.svg';?>" alt="Arrow down icon">Zwiń</a>
												</li>
											</ul>
										</div>
									<?php endif; ?>
								</div>
							</div>
						</div>
					<?php endwhile; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
	<?php if( have_rows('content_txtimg') ): ?>
		<div class="single-rows">
			<?php

			while( have_rows('content_txtimg') ): the_row(); ?>

			<?php $content_txt = get_sub_field('content_txt');
			if($content_txt):?>
			<div class="single-rows__text">
				<div class="grid-container grid-container--small">
					<?php echo $content_txt; ?>
				</div>
			</div>
		<?php endif; ?>

		<?php $image_full = get_sub_field('content_img_full');
		if($image_full):?>
		<div class="single-rows__img-full">
			<div class="grid-container">
				<img src="<?php echo esc_url($image_full['url']); ?>" alt="<?php echo esc_attr($image_full['alt']); ?>" />
			</div>
		</div>
	<?php endif; ?>

	<?php if( have_rows('content_img_half') ): ?>
		<div class="single-rows__img-half">
			<div class="grid-container">
				<div class="grid-x grid-margin-x grid-margin-y">
					<?php while( have_rows('content_img_half') ): the_row();
					$image_half = get_sub_field('content_img_half_item');
					?>
					<div class="small-12 medium-6 cell">
						<img src="<?php echo esc_url($image_half['url']); ?>" alt="<?php echo esc_attr($image_half['alt']); ?>" />
					</div>
				<?php endwhile; ?>
			</div>
		</div>
	</div>
<?php endif;?>

<?php $content_txt_full = get_sub_field('content_txt_full');
if($content_txt_full):?>
<div class="single-rows__text">
	<div class="grid-container">
		<?php echo $content_txt_full; ?>
	</div>
</div>
<?php endif; ?>

<?php endwhile; ?>
</div>
<?php endif;?>
</section> <!-- end article section -->

<footer class="article-footer">
	<?php wp_link_pages(); ?>
</footer> <!-- end article footer -->

</article> <!-- end article -->
