<?php
/**
 * Template part for displaying a single post
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
	<?php $thumbnail = get_the_post_thumbnail($post_id, 'full');?>
	<?php $header_class = $thumbnail ? 'article-header--default' : 'article-header--small';?>
	<header class="article-header <?php echo $header_class; ?>">
		<div class="grid-container">
			<h1 class="entry-title single-title" itemprop="headline"><?php the_title(); ?></h1>
			<?php get_template_part( 'parts/content', 'byline' ); ?>
		</div>
    </header> <!-- end article header -->

	<?php $entry_class = $thumbnail ? 'entry-content--default' : 'entry-content--upper';?>
    <section class="entry-content <?php echo $entry_class; ?>" itemprop="text">
		<div class="grid-container grid-container--small">
			<div class="single-thumbnail">
				<?php the_post_thumbnail('full'); ?>
			</div>
		</div>
		<?php if( have_rows('content_txtimg') ): ?>
			<div class="single-rows">
			<?php

			while( have_rows('content_txtimg') ): the_row(); ?>

				<?php $content_txt = get_sub_field('content_txt');
				if($content_txt):?>
				<div class="single-rows__text">
					<div class="grid-container grid-container--small">
						<?php echo $content_txt; ?>
					</div>
				</div>
				<?php endif; ?>

				<?php $image_full = get_sub_field('content_img_full');
				if($image_full):?>
				<div class="single-rows__img-full">
					<div class="grid-container">
						<img src="<?php echo esc_url($image_full['url']); ?>" alt="<?php echo esc_attr($image_full['alt']); ?>" />
					</div>
				</div>
				<?php endif; ?>

				<?php if( have_rows('content_img_half') ): ?>
					<div class="single-rows__img-half">
						<div class="grid-container">
							<div class="grid-x grid-margin-x grid-margin-y">
								<?php while( have_rows('content_img_half') ): the_row();
									$image_half = get_sub_field('content_img_half_item');
									?>
									<div class="small-12 medium-6 cell">
										<img src="<?php echo esc_url($image_half['url']); ?>" alt="<?php echo esc_attr($image_half['alt']); ?>" />
									</div>
								<?php endwhile; ?>
							</div>
						</div>
					</div>
				<?php endif;?>

				<?php $content_txt_full = get_sub_field('content_txt_full');
				if($content_txt_full):?>
				<div class="single-rows__text">
					<div class="grid-container">
						<?php echo $content_txt_full; ?>
					</div>
				</div>
				<?php endif; ?>

			<?php endwhile; ?>
			</div>
		<?php endif;?>
		<?php if( have_rows('post_accordion') ): ?>
			<div class="grid-container grid-container--small">
				<div class="post-accordion">
				<?php while( have_rows('post_accordion') ): the_row();
				$post_question = get_sub_field('post_question');
				$post_answer = get_sub_field('post_answer');
				?>

					<div class="characters-horizontal-desc-container">
						<ul class="accordion" data-slide-speed="500" data-accordion data-allow-all-closed="true">
							<li class="accordion-item" data-accordion-item>
								<a href="#" class="accordion-title"><?php echo $post_question;?></a>
								<div class="accordion-content" data-tab-content>
									<p><?php echo $post_answer;?></p>
								</div>
								<a href="#" class="accordion-arrow"><img src="<?php echo get_template_directory_uri() . '/assets/images/arrow-down.svg';?>" alt="Arrow down icon"></a>
							</li>
						</ul>
					</div>
				<?php endwhile; ?>
				</div>
			</div>
		<?php endif; ?>
	</section> <!-- end article section -->

	<?php $args = array(
		'post__not_in' => array(get_the_ID()),
	    'post_type' => 'post',
	    'posts_per_page' => 3,
	);
	$the_query = new WP_Query( $args ); ?>
	<?php if ( $the_query->have_posts() ) : ?>
        <!-- the loop -->
	<footer class="article-footer">
		<div class="article-footer-header">
			<div class="grid-container">
				<h2>zobacz również</h2>
			</div>
		</div>
		<div class="article-footer-main">
			<div class="grid-container">
				<div class="grid-x grid-margin-x">
					<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
						<div class="small-12 large-4 cell">
							<div class="section-main section-main--thumb">
								<div class="card section-main__card">
									<div class="section-main__img">
										<?php
										$thumbnail_id = get_post_thumbnail_id( $post->ID );
										$alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);
										the_post_thumbnail( 'full', array( 'alt' => $alt ) );
										?>
									</div>
									<div class="section-main__text">
										<h3><?php the_title(); ?></h3>
										<a class="button" href="<?php the_permalink(); ?>">zobacz</a>
									</div>
								</div>
							</div>
						</div>
		            <?php endwhile; ?>
		            <!-- end of the loop -->
				</div>
			</div>
		</div>
	    <?php wp_reset_postdata(); ?>
	</footer> <!-- end article footer -->
	<?php endif; ?>

</article> <!-- end article -->
