<?php
$args = array(
    'post_type' => 'aktualnosc',
    'posts_per_page' => 2
);
$the_query = new WP_Query( $args ); ?>

<?php if ( $the_query->have_posts() ) : ?>
    <section class="home-news section-main">
        <div class="grid-container">
            <div class="home-news__header section-main__header">
                <h2>Aktualności</h2>
                <a class="button button--small" href="/aktualnosci">więcej</a>
            </div>
            <div class="grid-x grid-margin-x">
                <!-- the loop -->
                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                    <div class="cell small-12 large-6">
                        <div class="card section-main__card">
                            <div class="home-news__img section-main__img">
                                <?php
                                $thumbnail_id = get_post_thumbnail_id( $post->ID );
                                $alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);
                                the_post_thumbnail( 'full', array( 'alt' => $alt ) );
                                ?>
                            </div>
                            <div class="home-news__text section-main__text">
                                <h3><?php the_title(); ?></h3>
                                <?php fl_excerpt($post->ID, 'fl_archive_short'); ?>
                                <a class="button" href="<?php the_permalink(); ?>">zobacz</a>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
                <!-- end of the loop -->
            </div>
        </div>
    </section>
    <?php wp_reset_postdata(); ?>
<?php endif; ?>
