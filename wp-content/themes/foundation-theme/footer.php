<?php
/**
 * The template for displaying the footer.
 *
 * Comtains closing divs for header.php.
 *
 * For more info: https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */
 ?>

 <?php
 $image = get_field('main_logo','option');
 ?>

				<footer class="footer" role="contentinfo">

                    <div class="footer__newsletter">
                        <h5>Zapisz się do naszego Newslettera</h5>
                        <h6>Bądz na bieżąco z działalnością Fundacji Lipińskiego</h6>
                        <div class="newletter-container">
                            <?php echo do_shortcode('[newsletter_form type="minimal"]'); ?>
                        </div>
                        <p>W każdym momencie możesz zrezygnować z subskrypcji newslettera. Zapoznaj się z <a href="/wp-content/uploads/2022/03/Fundacja_Lipinskiego_polityka_prywatnosci-.pdf" target="_blank">Polityką Prywatności</a>.</p>
                    </div>

                    <div class="grid-container">

                        <div class="inner-footer grid-x">

    						<div class="small-12 medium-12 large-12 cell">
                                <div class="footer__top">
                                    <a href="<?php echo home_url(); ?>"><img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>"></a>
                                </div>
    	    				</div>

    						<div class="small-12 medium-12 large-12 cell">
                                <div class="footer__bottom">
                                    <p class="source-org copyright">copyright <?php echo date('Y'); ?> <?php bloginfo('name'); ?></p>
                                    <div class="footer__bottom-right">
                                        <nav role="navigation">
            	    						<?php joints_footer_links(); ?>
            	    					</nav>
                                    </div>
                                </div>
    						</div>

    					</div> <!-- end #inner-footer -->

                    </div>

				</footer> <!-- end .footer -->

			</div>  <!-- end .off-canvas-content -->

		</div> <!-- end .off-canvas-wrapper -->

		<?php wp_footer(); ?>

	</body>

</html> <!-- end page -->
