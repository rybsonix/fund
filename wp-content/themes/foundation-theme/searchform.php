<?php
/**
 * The template for displaying search form
 */
 ?>

<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
	<label aria-labelledby="search">
		<input type="search" class="search-field" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Wyszukaj:', 'jointswp' ) ?>" />
        <i class="fas fa-search"></i>
        <input type="submit" class="search-submit button" value="" />
	</label>
</form>
