<?php
/*
Template Name: Podstrona - Autor - Opis (obok)
*/

get_header(); ?>

<div class="content single-page-person single-page-publish-horizontal">

	<div class="inner-content grid-x grid-margin-x">

		<main class="main small-12 cell" role="main">

		    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		    	<?php get_template_part( 'parts/loop', 'publish-horizontal' ); ?>

		    <?php endwhile; else : ?>

		   		<?php get_template_part( 'parts/content', 'missing' ); ?>

		    <?php endif; ?>

		</main> <!-- end #main -->

	</div> <!-- end #inner-content -->

</div> <!-- end #content -->

<?php get_footer(); ?>
